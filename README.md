# Ember.js TodoMVC Example using Ember CLI

> A framework for creating ambitious web applications.

> _[Ember.js - emberjs.com](http://emberjs.com)_
> _[Ember CLI - ember-cli.com](http://ember-cli.com)_

## Credits
https://github.com/tastejs/todomvc
